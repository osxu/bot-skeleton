Bot Skeleton for a simple irc bot written in C
by osxu

usage:
-h              - shows this help
-v              - verbose mode  
-r <new_name>   - rename process  
-n <nick>       - set bot nick   
-c '#<channel>' - set channel with quotes and #  
-s <host>       - set host server   
-p <port>       - set port (default 6667)  
-a <authpass>   - password for NickServ identify   
-k <chanpass>   - channel password  
example: ./bot -v -n mybot -c '#mychan' -s irc.freenode.net -r 'http -DSSL' -a '#my.secret_pw' -k 'my.chan#pw'

TODO:  
add setting channel and user modes  
add support for CTCP messages  
(add support for creating, registering channels)  

BUGS:  
rev_str seems not to work properly

v1.0  
+ redesigned code using header files and seperate source code

v0.4  
+ add irc authentication  
+ cli args -a for providing the authentication password  
+ add channel password support  
+ cli arg -k for providing the channel password  

v0.3  
+ rev_str function for realname  
+ sig handler 4 clean exit of the bot  

v0.2  
+ cli args -n -c -s -p for setting name, channel, host and port  
+ reprogrammed -r option to use environment copy to get more alloc mem 4 argv[0]  

v0.1  
+ basic skeleton  
+ parser function for public and private messages  
+ cli args -h -v -r for help, verbose and renaming  
