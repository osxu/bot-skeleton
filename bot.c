#include "functions.h"


int main(int argc, char **argv) {

    // default value 
    strcpy(port,"6667");
    
    // handle some signals clean
    static struct sigaction act;

    act.sa_handler = sig_handler;
    act.sa_flags = 0;

    sigfillset(&(act.sa_mask));
    sigaction( SIGTERM, &act, NULL );
    sigaction( SIGKILL  , &act, NULL );
    sigaction( SIGINT, &act, NULL );
    sigaction( SIGHUP, &act, NULL );

    if(argc < 2)
        help(argv[0]);


    char *user, *command, *where, *message, *sep, *target, *renamemyselfto = NULL, *rnick;
    int i, j, l, sl, o = -1, start, wordcount; 
    char buf[513];
    struct addrinfo hints, *res;

    // parse args
    int c;
    opterr = 0;

    while ((c = getopt (argc, argv, "hvr:n:c:s:p:a:k:")) != -1)
        switch (c)
        {
            case 'h':
                help(argv[0]);
                break;
            case 'v':
                debug = true;
                break;
            case 'r':
                renamemyselfto = optarg;
                break;
            case 'n':
                strncpy(nick,optarg,256);
                break;
            case 'c':
                strncpy(channel,optarg,256); 
                break;
            case 's':
                strncpy(host,optarg,256); 
                break;
            case 'p':
                strncpy(port,optarg,5); 
                break;    
            case 'a':
                strncpy(authpass,optarg,256); 
                break;    
            case 'k':
                strncpy(chanpass,optarg,256); 
                break;    
            default:
                help(argv[0]);
        } 

    if(nick == NULL || channel == NULL || host == NULL){
        printf("ERROR: please define a nick, channel and host \n");
        exit (0);
    }

    if(renamemyselfto != NULL)
        renameme(argv,argc,renamemyselfto);
    else
        renameme(argv,argc,argv[0]);

    // socket
    memset(&hints, 0, sizeof hints);
    hints.ai_family = AF_INET;
    hints.ai_socktype = SOCK_STREAM;
    getaddrinfo(host, port, &hints, &res);
    conn = socket(res->ai_family, res->ai_socktype, res->ai_protocol);
    connect(conn, res->ai_addr, res->ai_addrlen);
  
    // set inital user and nick
    rnick = strdup(nick);
    rev_str(rnick);
    raw("USER %s +i 0 :%s\r\n", nick, rnick);
    setnick(nick);

    if(authpass != NULL){
        sendpriv("NickServ","identify %s",authpass);
        printf("AUTH: Try to identify user %s with pass %s\n",nick,authpass);
    }
    
    while ((sl = read(conn, sbuf, 512))) {
        for (i = 0; i < sl; i++) {
            o++;
            buf[o] = sbuf[i];
            if ((i > 0 && sbuf[i] == '\n' && sbuf[i - 1] == '\r') || o == 512) {
                buf[o + 1] = '\0';
                l = o;
                o = -1;
               
                if(debug) 
                    printf(">> %s", buf);
               
                // PING PONG 
                if (!strncmp(buf, "PING", 4)) {
                    buf[1] = 'O';
                    raw(buf);
                // its a PRIVMSG or NOTICE -> parsing response 
                } else if (buf[0] == ':') {
                    wordcount = 0;
                    user = command = where = message = NULL;
                    for (j = 1; j < l; j++) {
                        if (buf[j] == ' ') {
                            buf[j] = '\0';
                            wordcount++;
                            switch(wordcount) {
                                case 1: user = buf + 1; break;
                                case 2: command = buf + start; break;
                                case 3: where = buf + start; break;
                            }
                            if (j == l - 1) continue;
                            start = j + 1;
                        } else if (buf[j] == ':' && wordcount == 3) {
                            if (j < l - 1) message = buf + j + 1;
                            break;
                        }
                    }
                   
                    // the response seems not to be valid 
                    if (wordcount < 2) continue;
                   
                    if (!strncmp(command, "001", 3) && channel != NULL) {
                        joinchan(channel,chanpass);
                    } else if (!strncmp(command, "PRIVMSG", 7) || !strncmp(command, "NOTICE", 6)) {
                        if (where == NULL || message == NULL) continue;
                        // this cuts of user!host to user only
                        if ((sep = strchr(user, '!')) != NULL) user[sep - user] = '\0';
                        // check for channel modes to set target 
                        if (where[0] == '#' || where[0] == '&' || where[0] == '+' || where[0] == '!') target = where; else target = user;
                        printf("[from: %s] [reply-with: %s] [where: %s] [reply-to: %s] %s", user, command, where, target, message);
                        // msg goes to myself
                        if(!strcmp(where,nick)){
                            parsemsgpriv(user,message);
                        // msg goes to channel
                        }else if(!strcmp(where,channel)){
                            parsemsgpub(message);
                        }
                    }
                }
                
            }
        }
        
    }
    return 0;
}
