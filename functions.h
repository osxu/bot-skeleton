#ifndef FUNCTIONS_H
#define FUNCTIONS_H

#define version "v1.0"
#define sourcecodeurl "https://bitbucket.org/osxu/bot-skeleton/"

#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <netdb.h>
#include <stdarg.h>
#include <stdbool.h>
#include <stdlib.h>
#include <signal.h>

extern char **environ;
extern bool debug;

int conn;
char sbuf[512];
char sbuf2[512];
char nick[256];
char channel[256];
char host[256];
char port[5];
char authpass[256];
char chanpass[256];

void help(char *);
void irchelp();
void raw(char *,...);
void sendpriv(char *,char *,...);
void parsemsgpriv(char *,char *);
void parsemsgpub(char *);
void setnick(char *);
void joinchan(char *,char *);
void renameme(char **,int,char *);
void rev_str(char *);
void sig_handler();

#endif /* FUNCTIONS_H */
