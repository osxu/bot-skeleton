CC = gcc
CFLAGS = -Wall -s
DEPS = functions.h
OBJ = bot.o functions.o

%.o: %.c $(DEPS)
	        $(CC) $(CFLAGS) -c -o $@ $<

bot: $(OBJ)
	        gcc $(CFLAGS) -o $@ $^

clean:
	rm *.o
