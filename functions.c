#include "functions.h"

bool debug = false;

void help(char *argv0){
    printf("Bot Skeleton %s\n",version);
    printf("usage:\n");
    printf("-h\t\t- shows this help\n");
    printf("-v\t\t- verbose mode\n");
    printf("-r <new_name>\t- rename process\n");
    printf("-n <nick>\t- set bot nick \n");
    printf("-c '#<channel>'\t- set channel with quotes and #\n");
    printf("-s <host>\t- set host server \n");
    printf("-p <port>\t- set port (default 6667)\n");
    printf("-a <authpass>\t- password for NickServ identify \n");
    printf("-k <chanpass>\t- channel password\n");
    printf("example: %s -v -n mybot -c '#mychan' -s irc.freenode.net -r 'http -DSSL' -a '#my.secret_pw' -k 'my.chan#pw'\n",argv0);
    exit (0);
}

// help written by the !help cmd
void irchelp(){
    sendpriv(channel,"hello im %s, a bot skelleton %s written in C",nick,version);
    sendpriv(channel,"you can find my sourcode here: %s",sourcecodeurl);
    sendpriv(channel,"write !%s to the channel",nick);
    sendpriv(channel,"or send me a private message with !egg");
}

/* function raw
 * write print formated msg to socket
 */
void raw(char *fmt, ...) {
    va_list ap;
    va_start(ap, fmt);
    vsnprintf(sbuf, 512, fmt, ap);
    va_end(ap);
    if(debug)
        printf("<< %s", sbuf);
    write(conn, sbuf, strlen(sbuf));
}

/* shortcut function for sending messages
 * uses va_list feature for string formation
 */
void sendpriv(char *where, char *msg, ...){
    va_list ap;
    va_start(ap, msg);
    vsnprintf(sbuf2, 512, msg, ap);
    va_end(ap);
    raw("PRIVMSG %s :%s\r\n",where,sbuf2);
}

// parse private messages
void parsemsgpriv(char *user, char *msg){
    // add your code parsed by private messages here
    if(!strncmp(msg,"!egg",4)){
        sendpriv(user,"here is your ()<- Egg");
    }
}

// parse public messages
void parsemsgpub(char *msg){
    // add your code parsed by channel messages here
    // msg !<nick>
    
    char magicnick[strlen(nick)+1];
    strcpy(magicnick,"!");
    strcat(magicnick,nick);

    if(!strncmp(msg,magicnick,strlen(magicnick)))
        sendpriv(channel,"hi im %s, i do not drop eggs 4 u",nick);
    else if(!strncmp(msg,"!help",5)){
        irchelp();
    }
}

// set nick
void setnick(char *nick){
    raw("NICK %s\r\n", nick);
}

// join a channel
void joinchan(char *channel, char *pass){
    if( pass == NULL)
        raw("JOIN %s\r\n", channel);
    else
        raw("JOIN %s %s\r\n", channel,pass);
}

// change program name
void renameme(char **argv,int argc,char *newname){
    // copy newname bevor moving environment
    char name[strlen(newname)];
    strcpy(name,newname);

    // checking environment length
    int env_len = -1;
    if(environ)
        while (environ[++env_len])
            ;
    // set size of args
    unsigned int size;
    if(env_len > 0)
        size = environ[env_len-1] + strlen(environ[env_len-1]) - argv[0];
    else
        size = argv[argc-1] + strlen(argv[argc-1]) -argv[0];
    // moving old environment to new for "reallocating" the memory
    if(environ){
        char **new_environ = malloc(env_len*sizeof(char*));
        unsigned int i = -1;
        while(environ[++i])
            new_environ[i] = strdup(environ[i]);
        environ = new_environ;
    }
    // overwrite args
    char *args = argv[0];
    memset(args, '\0', size);
    snprintf(args,size - 1, "%s",name);
}

// reverse a string
void rev_str(char str[]){
    int len = sizeof(str)-1;
    char *reverse = strdup(str);
    int i = -1;
    while (str[++i]){
        reverse[len - i] = str[i];
    }
    strcpy(str,reverse);
}

// signal handler to cleanly QUIT
void sig_handler(){
    raw("QUIT\r\n");
    exit (0);
}
